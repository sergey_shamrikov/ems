$(document).ready(function () {

	/* кастомный селект в форме */

	$(function () {
		$('.b-formSelect').selectbox();
	});

	/* Устанавливаем высоту контента */

	var height = $('.b-contentItem.b-active').outerHeight();
	$('.b-content').css({ 'height': height });

	$('.b-contentItem.b-active').css({ 'opacity': '1' });

	/* Активность меню */

	$('.b-topMenuItem').click(function () {
		if ($(this).hasClass('b-active')) return false;

		var index = $(this).index();
		$('.b-topMenuItem').removeClass('b-active');
		$('.b-topMenuItem').eq(index).addClass('b-active');
		$('.b-contentItem.b-active').animate({ 'opacity': '0' }, 300, function () { $(this).removeClass('b-active'); });
		var height = $('.b-contentItem').eq(index).outerHeight();
		$('.b-content').stop().animate({ 'height': height }, 1000);
		$('.b-contentItem').eq(index).addClass('b-active').animate({ 'opacity': '1' }, 500);

		if (index === 0) {
			window.location.hash = 'bf';
			$('.b-wrapper').css({ 'background': 'url(img/b-wrapper.jpg) no-repeat center top', 'padding': '0 0 30px 0' });
			$('.b-contentItemRight').css({ 'background': 'transparent' }).stop().animate({ 'top': '110px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'rgba(255,255,255,0.85)' });
			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.b-contentHolder').css({ 'background': '#fff' });
				}
			}
			$('.b-partnersWrapper').css({ 'background': '#003366' });
			$('.b-content').css({ 'border-right': '1px solid #ccc' });
			$('.b-top').css({ 'display': 'none' });
			$('.b-wrapper').css({ 'margin-top': '86px' });
			$('.b-wrapper').animate({ 'margin-top': '-1px' }, 500);
			$('.b-top').removeClass('b-active');
			$('body, html').animate({ scrollTop: '55px' }, 500);
		}

		if (index === 1) {
			$('.b-radioInput.-js-packet1').click();
			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.pseudo-radio').css({'background': 'url(img/checkbox.png) left top no-repeat'});
					$('.-js-packet1').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
				}
			}
			window.location.hash = 'store';
			$(this).stop().animate({ 'background-color': '#003366' }, 300);
			$('.b-wrapper , .b-partnersWrapper').css({ 'background': '#003366', 'padding': '0' });
			$('.b-contentButton').css({ 'color': '#003366' });
			$('.b-contentItemRight').css({ 'background': '#fff' }).stop().animate({ 'top': '215px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'transparent' });
			$('.b-content').css({ 'border': '0' });
			if ($('.b-top').hasClass('b-active')) return false;
			$('.b-wrapper').animate({ 'margin-top': '86px' }, 500, function () {
				$('.b-top').css({ 'display': 'block' });
				$('.b-wrapper').css({ 'margin-top': '-1px' });
				$('.b-top').addClass('b-active');
			});
			$('body, html').animate({ scrollTop: '55px' }, 500);

		}
		if (index === 2) {

			$('.b-radioInput.-js-packet2').click();
			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.pseudo-radio').css({'background': 'url(img/checkbox.png) left top no-repeat'});
					$('.-js-packet2').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
				}
			}
			window.location.hash = 'buisness';
			$(this).stop().animate({ 'background-color': '#009e49' }, 300);
			$('.b-wrapper , .b-partnersWrapper').css({ 'background': '#009e49', 'padding': '0' });
			$('.b-contentButton').css({ 'color': '#009e49' });
			$('.b-contentItemRight').css({ 'background': '#fff' }).stop().animate({ 'top': '215px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'transparent' });
			$('.b-content').css({ 'border': '0' });
			if ($('.b-top').hasClass('b-active')) return false;
			$('.b-wrapper').animate({ 'margin-top': '86px' }, 500, function () { $('.b-top').css({ 'display': 'block' }); $('.b-wrapper').css({ 'margin-top': '-1px' }); $('.b-top').addClass('b-active'); });
			$('body, html').animate({ scrollTop: '55px' }, 500);

		}
		if (index === 3) {
			$('.b-radioInput.-js-packet3').click();
			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.pseudo-radio').css({'background': 'url(img/checkbox.png) left top no-repeat'});
					$('.-js-packet3').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
				}
			}
			window.location.hash = 'durability';
			$(this).stop().animate({ 'background-color': '#009cff' }, 300);
			$('.b-wrapper , .b-partnersWrapper').css({ 'background': '#009cff', 'padding': '0' });
			$('.b-contentButton').css({ 'color': '#009cff' });
			$('.b-contentItemRight').css({ 'background': '#fff' }).stop().animate({ 'top': '215px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'transparent' });
			$('.b-content').css({ 'border': '0' });
			if ($('.b-top').hasClass('b-active')) return false;
			$('.b-wrapper').animate({ 'margin-top': '86px' }, 500, function () { $('.b-top').css({ 'display': 'block' }); $('.b-wrapper').css({ 'margin-top': '-1px' }); $('.b-top').addClass('b-active'); });
			$('body, html').animate({ scrollTop: '55px' }, 500);
		}
		if (index === 4) {
			$('.b-radioInput.-js-packet4').click();
			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.pseudo-radio').css({'background': 'url(img/checkbox.png) left top no-repeat'});
					$('.-js-packet4').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
				}
			}
			window.location.hash = 'perfection';
			$(this).stop().animate({ 'background-color': '#68217a' }, 300);
			$('.b-wrapper , .b-partnersWrapper').css({ 'background': '#68217a', 'padding': '0' });
			$('.b-contentButton').css({ 'color': '#68217a' });
			$('.b-contentItemRight').css({ 'background': '#fff' }).stop().animate({ 'top': '215px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'transparent' });
			$('.b-content').css({ 'border': '0' });
			if ($('.b-top').hasClass('b-active')) return false;
			$('.b-wrapper').animate({ 'margin-top': '86px' }, 500, function () { $('.b-top').css({ 'display': 'block' }); $('.b-wrapper').css({ 'margin-top': '-1px' }); $('.b-top').addClass('b-active'); });
			$('body, html').animate({ scrollTop: '55px' }, 500);
		}
		if (index === 5) {
			$('.b-radioInput.-js-packet5').click();
			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.pseudo-radio').css({'background': 'url(img/checkbox.png) left top no-repeat'});
					$('.-js-packet5').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
				}
			}
			window.location.hash = 'app';
			$(this).stop().animate({ 'background-color': '#b4009e' }, 300);
			$('.b-wrapper , .b-partnersWrapper').css({ 'background': '#b4009e', 'padding': '0' });
			$('.b-contentButton').css({ 'color': '#b4009e' });
			$('.b-contentItemRight').css({ 'background': '#fff' }).stop().animate({ 'top': '215px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'transparent' });
			$('.b-content').css({ 'border': '0' });
			if ($('.b-top').hasClass('b-active')) return false;
			$('.b-wrapper').animate({ 'margin-top': '86px' }, 500, function () { $('.b-top').css({ 'display': 'block' }); $('.b-wrapper').css({ 'margin-top': '-1px' }); $('.b-top').addClass('b-active'); });
			$('body, html').animate({ scrollTop: '55px' }, 500);
		}
	});



	/* Переходы при заходе на сайт */

	var initUrl = function () {
		if (window.location.hash === '#bf') {
			$('.b-wrapper').css({ 'background': 'url(img/b-wrapper.jpg) no-repeat center top', 'padding': '0 0 30px 0' });
			$('.b-contentItemRight').css({ 'background': 'transparent', 'top': '110px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'rgba(255,255,255,0.85)' });
			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.b-contentHolder').css({ 'background': '#fff' });
				}
			}
			$('.b-partnersWrapper').css({ 'background': '#003366' });
			$('.b-content').css({ 'border-right': '1px solid #ccc' });
			$('.b-top').css({ 'display': 'none' });
			$('.b-wrapper').css({ 'margin-top': '86px' });
			$('.b-wrapper').animate({ 'margin-top': '-1px' }, 0);
			$('.b-top').removeClass('b-active');

		}


		if (window.location.hash === '#store') {
			var index = 1;
			$('.b-topMenuItem').removeClass('b-active');
			$('.b-topMenuItem').eq(index).addClass('b-active');
			$('.b-contentItem.b-active').animate({ 'opacity': '0' }, 0, function () { $(this).removeClass('b-active'); });
			var height = $('.b-contentItem').eq(index).outerHeight();
			$('.b-content').animate({ 'height': height }, 0);
			$('.b-contentItem').eq(index).addClass('b-active').animate({ 'opacity': '1' }, 0);

			$('.b-radioInput.-js-packet1').click();

			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.-js-packet1').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
				}
			}

			$('.b-wrapper , .b-partnersWrapper').css({ 'background': '#003366', 'padding': '0' });
			$('.b-contentButton').css({ 'color': '#003366' });
			$('.b-contentItemRight').css({ 'background': '#fff', 'top': '215px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'transparent' });
			$('.b-content').css({ 'border': '0' });
			if ($('.b-top').hasClass('b-active')) return false;
			$('.b-wrapper').animate({ 'margin-top': '86px' }, 0, function () { $('.b-top').css({ 'display': 'block' }); $('.b-wrapper').css({ 'margin-top': '-1px' }); $('.b-top').addClass('b-active'); });
			$('body, html').animate({ scrollTop: '55px' }, 500);
		}


		if (window.location.hash === '#buisness') {
			var index = 2;
			$('.b-topMenuItem').removeClass('b-active');
			$('.b-topMenuItem').eq(index).addClass('b-active');
			$('.b-contentItem.b-active').animate({ 'opacity': '0' }, 0, function () { $(this).removeClass('b-active'); });
			var height = $('.b-contentItem').eq(index).outerHeight();
			$('.b-content').animate({ 'height': height }, 0);
			$('.b-contentItem').eq(index).addClass('b-active').animate({ 'opacity': '1' }, 0);

			$('.b-radioInput.-js-packet2').click();

			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.-js-packet2').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
				}
			}

			$('.b-wrapper , .b-partnersWrapper').css({ 'background': '#009e49', 'padding': '0' });
			$('.b-contentButton').css({ 'color': '#009e49' });
			$('.b-contentItemRight').css({ 'background': '#fff', 'top': '215px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'transparent' });
			$('.b-content').css({ 'border': '0' });
			if ($('.b-top').hasClass('b-active')) return false;
			$('.b-wrapper').animate({ 'margin-top': '86px' }, 0, function () { $('.b-top').css({ 'display': 'block' }); $('.b-wrapper').css({ 'margin-top': '-1px' }); $('.b-top').addClass('b-active'); });
			$('body, html').animate({ scrollTop: '55px' }, 500);
		}


		if (window.location.hash === '#durability') {
			var index = 3;
			$('.b-topMenuItem').removeClass('b-active');
			$('.b-topMenuItem').eq(index).addClass('b-active');
			$('.b-contentItem.b-active').animate({ 'opacity': '0' }, 0, function () { $(this).removeClass('b-active'); });
			var height = $('.b-contentItem').eq(index).outerHeight();
			$('.b-content').animate({ 'height': height }, 0);
			$('.b-contentItem').eq(index).addClass('b-active').animate({ 'opacity': '1' }, 0);

			$('.b-radioInput.-js-packet3').click();

			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.-js-packet3').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
				}
			}

			$('.b-wrapper , .b-partnersWrapper').css({ 'background': '#009cff', 'padding': '0' });
			$('.b-contentButton').css({ 'color': '#009cff' });
			$('.b-contentItemRight').css({ 'background': '#fff', 'top': '215px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'transparent' });
			$('.b-content').css({ 'border': '0' });
			if ($('.b-top').hasClass('b-active')) return false;
			$('.b-wrapper').animate({ 'margin-top': '86px' }, 0, function () { $('.b-top').css({ 'display': 'block' }); $('.b-wrapper').css({ 'margin-top': '-1px' }); $('.b-top').addClass('b-active'); });
			$('body, html').animate({ scrollTop: '55px' }, 500);
		}


		if (window.location.hash === '#perfection') {
			var index = 4;
			$('.b-topMenuItem').removeClass('b-active');
			$('.b-topMenuItem').eq(index).addClass('b-active');
			$('.b-contentItem.b-active').animate({ 'opacity': '0' }, 0, function () { $(this).removeClass('b-active'); });
			var height = $('.b-contentItem').eq(index).outerHeight();
			$('.b-content').animate({ 'height': height }, 0);
			$('.b-contentItem').eq(index).addClass('b-active').animate({ 'opacity': '1' }, 0);

			$('.b-radioInput.-js-packet4').click();

			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.-js-packet4').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
				}
			}

			$('.b-wrapper , .b-partnersWrapper').css({ 'background': '#68217a', 'padding': '0' });
			$('.b-contentButton').css({ 'color': '#68217a' });
			$('.b-contentItemRight').css({ 'background': '#fff', 'top': '215px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'transparent' });
			$('.b-content').css({ 'border': '0' });
			if ($('.b-top').hasClass('b-active')) return false;
			$('.b-wrapper').animate({ 'margin-top': '86px' }, 0, function () { $('.b-top').css({ 'display': 'block' }); $('.b-wrapper').css({ 'margin-top': '-1px' }); $('.b-top').addClass('b-active'); });
			$('body, html').animate({ scrollTop: '55px' }, 500);
		}


		if (window.location.hash === '#turbo') {
			var index = 5;
			$('.b-topMenuItem').removeClass('b-active');
			$('.b-topMenuItem').eq(index).addClass('b-active');
			$('.b-contentItem.b-active').animate({ 'opacity': '0' }, 0, function () { $(this).removeClass('b-active'); });
			var height = $('.b-contentItem').eq(index).outerHeight();
			$('.b-content').animate({ 'height': height }, 0);
			$('.b-contentItem').eq(index).addClass('b-active').animate({ 'opacity': '1' }, 0);

			$('.b-radioInput.-js-packet5').click();

			if ($.browser.msie) {
				if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
					$('.-js-packet5').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
				}
			}

			$('.b-wrapper , .b-partnersWrapper').css({ 'background': '#b4009e', 'padding': '0' });
			$('.b-contentButton').css({ 'color': '#b4009e' });
			$('.b-contentItemRight').css({ 'background': '#fff', 'top': '215px' }, 500);
			$('.b-contentHolder').css({ 'padding': '0 30px 30px 30px', 'background': 'transparent' });
			$('.b-content').css({ 'border': '0' });
			if ($('.b-top').hasClass('b-active')) return false;
			$('.b-wrapper').animate({ 'margin-top': '86px' }, 0, function () { $('.b-top').css({ 'display': 'block' }); $('.b-wrapper').css({ 'margin-top': '-1px' }); $('.b-top').addClass('b-active'); });
			$('body, html').animate({ scrollTop: '55px' }, 500);
		}
	}

	setTimeout(function () {
		initUrl();
	}, 100);


	/* Фикс для iOS для чекбоксов */

	var deviceAgent = navigator.userAgent.toLowerCase();
	var iOS = deviceAgent.match(/(iphone|ipod|ipad)/);
	if (iOS) {
		$('label').click(function (event) {
			$('#' + $(event.target).attr('for')).attr('checked', true).change();
		});
	}

	/* Активность инпутов в форме */

	$('.b-formInput').each(function () {
		if ($(this).val()) {
			$(this).parent().find('.b-formLabel').css({ 'display': 'none' });
		}
	});

	$('.b-formText').each(function () {
		if ($(this).val()) {
			$(this).parent().find('.b-formLabel').css({ 'display': 'none' });
		}
	});


	$('.b-formInput').focus(function () {
		$(this).parent().find('.b-formLabel').css({ 'display': 'none' });
	});

	$('.b-formInput').blur(function () {
		if ($(this).val()) return false;
		$(this).parent().find('.b-formLabel').css({ 'display': 'block' });
	});

	$('.b-formText').focus(function () {
		$(this).parent().find('.b-formLabel').css({ 'display': 'none' });
	});

	$('.b-formText').blur(function () {
		if ($(this).val()) return false;
		$(this).parent().find('.b-formLabel').css({ 'display': 'block' });
	});

	$('.b-formLabel').click(function () {
		$(this).css({ 'display': 'none' });
		$(this).parent().find('.b-formInput').focus();
		$(this).parent().find('.b-formText').focus();
	});

	/* Показываем лайтбокс */
	var bodyHeight = $('body').outerHeight();

	$('.b-contentButton').click(function () {

		$('body , html').animate({ scrollTop: '160px' }, 500);
		var index = $(this).parent().parent().index();
		$('body .mstSrc_Suggestions').css({'z-index':'10'});
		$('.b-lightboxHolder , .b-lightboxBack , .b-lightboxRight').css({ 'display': 'block' });
		$('.b-lightboxItem').eq(index - 1).css({ 'display': 'block' });
		$('.b-lightboxHolder , .b-lightboxBack , .b-lightboxRight').animate({ 'opacity': '1' }, 300);
		$('.b-lightboxItem').eq(index - 1).animate({ 'opacity': '1' }, 300);

		if (  $('.b-lightboxHolder').outerHeight()+275 > bodyHeight ) {
			var lightboxheight = $('.b-lightboxHolder').outerHeight();
			$('body').css({'height':lightboxheight+275});
			$('.b-lightboxBack').css({'height':lightboxheight+355});
		}
		else {
			$('body').css({'height':bodyHeight+20});
			$('.b-lightboxBack').css({'height':bodyHeight+100});
		}
	});

	/* Скрываем лайтбокс */

	$('.b-lightboxClose , .b-lightboxCloseBottom , .b-lightboxBack').click(function () {
		$('.b-lightboxRight , .b-lightboxBack , .b-lightboxHolder , .b-lightboxItem').animate({ 'opacity': '0' }, 300, function () {
			$(this).css({ 'display': 'none' });
			$('.b-lightboxItem').css({ 'display': 'none' });
			$('body .mstSrc_Suggestions').css({'z-index':'999'});
			$('body').css({'height':bodyHeight});
		});
	});


	/* Ховеры кнопок на главной */

	$('.b-bottom .b-topMenuItem').hover(
		function () {
			var colorIndex = $(this).index();
			if (colorIndex === 1) {
				$(this).stop().animate({ 'background-color': '#022345' }, 300);
			}
			if (colorIndex === 2) {
				$(this).stop().animate({ 'background-color': '#017235' }, 300);
			}
			if (colorIndex === 3) {
				$(this).stop().animate({ 'background-color': '#057bc6' }, 300);
			}
			if (colorIndex === 4) {
				$(this).stop().animate({ 'background-color': '#531a61' }, 300);
			}
			if (colorIndex === 5) {
				$(this).stop().animate({ 'background-color': '#9c0289' }, 300);
			}
		},
		function () {
			var colorIndex = $(this).index();
			if (colorIndex === 1) {
				$(this).stop().animate({ 'background-color': '#003366' }, 300);
			}
			if (colorIndex === 2) {
				$(this).stop().animate({ 'background-color': '#009e49' }, 300);
			}
			if (colorIndex === 3) {
				$(this).stop().animate({ 'background-color': '#009cff' }, 300);
			}
			if (colorIndex === 4) {
				$(this).stop().animate({ 'background-color': '#68217a' }, 300);
			}
			if (colorIndex === 5) {
				$(this).stop().animate({ 'background-color': '#b4009e' }, 300);
			}
		}
);

	/* Ховеры кнопок на Не главной */

	$('.b-top .b-topMenuItem').hover(
			function () {
				var colorIndex = $(this).index();
				if ($(this).hasClass('b-active')) return false;
				if (colorIndex === 1) {
					$(this).stop().animate({ 'background-color': '#022345' }, 300);
				}
				if (colorIndex === 2) {
					$(this).stop().animate({ 'background-color': '#017235' }, 300);
				}
				if (colorIndex === 3) {
					$(this).stop().animate({ 'background-color': '#057bc6' }, 300);
				}
				if (colorIndex === 4) {
					$(this).stop().animate({ 'background-color': '#531a61' }, 300);
				}
				if ( colorIndex === 5 ) {
	    			$(this).stop().animate({'background-color':'#9c0289'} , 300);
	    		}
			},
			function () {
				var colorIndex = $(this).index();
				if (colorIndex === 1) {
					$(this).stop().animate({ 'background-color': '#003366' }, 300);
				}
				if (colorIndex === 2) {
					$(this).stop().animate({ 'background-color': '#009e49' }, 300);
				}
				if (colorIndex === 3) {
					$(this).stop().animate({ 'background-color': '#009cff' }, 300);
				}
				if (colorIndex === 4) {
					$(this).stop().animate({ 'background-color': '#68217a' }, 300);
				}
				if ( colorIndex === 5 ) {
	    			$(this).stop().animate({'background-color':'#b4009e'} , 300);
	    		}
			}
	);

	/* Валидация формы */

	$('.b-formInput.phone').focus(function () {
		if ($('.b-formInput.phone').val()) return false;
		$('.b-formInput.phone').val('+7');
	});


	$('.phone').mask('+9 999 999 99 99');

	$('.b-formButton').click(function () {



		if ($("#aspnetForm").valid(
			{
			ignore: ".novalidate , input[type='text']:hidden",
			rules: {
				name: "required",
				phone: "required",
				radio: "radio",
				check: "check"
			}
		})


		) { 
			SendAtlas('http://view.atdmt.com/iaction/mrrrut_FY14Businessboostdec2013PrdConsultation_1'); 
			$('.b-formButton').addClass('b-formButton_disabled').css({'cursor':'default'}).animate({ 'background-color': '#ccc' }, 300);
			setTimeout(function(){
				$("#aspnetForm").submit();
			}, 3000);
		}
		else {
			$('.b-formInput.error').parent().addClass('error');
			$('.b-formInput.phone.error').parent().find('.b-formLabel').text('Телефон в виде +7xxxxxxxxxx');
			$('.b-formBlock label.error').addClass('nodisplay');
			$('.b-agreement .b-checkbox1 , .b-formButton').click(function () {
				setTimeout(function () { $('.b-agreementCLear label.error').text('Необходимо ваше согласие на обработку данных'); }, 1);

			});

			if ($('.b-checkInput1').hasClass('error')) { $('.b-agreement').addClass('error'); }
			else { $('.b-agreement').removeClass('error'); }
			$('.b-agreement label.error').appendTo('.b-agreementCLear').text('Необходимо ваше согласие на обработку данных');
			$('.b-radio label.error').appendTo('.b-radioBottom');
		}

	});

	/* Галка согласия в форме по умолчанию включена */

	// $('.b-checkInput1').click();

	/* Меняем стандартный текст в селекте */

	setTimeout(function () {
		$('.text').text('Москва и МО*');
	}, 1);


	/* Ховеры кнопок формы */

	$('.b-formButton').hover(
		function () {
			if($(this).hasClass('b-formButton_disabled')) return;
			$(this).stop().animate({ 'background-color': '#45157E' }, 300);
		},
		function () {
			if($(this).hasClass('b-formButton_disabled')) return;
			$(this).stop().animate({ 'background-color': '#760A85' }, 300);
		}
	);

	$('.b-successBack').hover(
		function () {
			$(this).stop().animate({ 'background-color': '#aa3a0f' }, 300);
		},
		function () {
			$(this).stop().animate({ 'background-color': '#f05013' }, 300);
		}
	);

	/* Переходы по меню после успешной отправки формы */

	$('.b-successBack').click(function () {
		if (window.location.hash === '') { location.href = "./" }
		if (window.location.hash === '#bf') { location.href = "./#bf" }
		if (window.location.hash === '#store') { location.href = "./#store" }
		if (window.location.hash === '#buisness') { location.href = "./#buisness" }
		if (window.location.hash === '#durability') { location.href = "./#durability" }
		if (window.location.hash === '#perfection') { location.href = "./#perfection" }
		if (window.location.hash === '#turbo') { location.href = "./#turbo" }
	});

	/* Ховер кнопки закрытия лайтбокса */

	$('.b-lightboxCloseBottom').hover(
		function () {
			$(this).stop().animate({ 'background-color': '#636363' }, 300);
		},
		function () {
			$(this).stop().animate({ 'background-color': '#999999' }, 300);
		}
	);

	/* Ховер кнопки открытия лайтбокса */

	$('.b-contentButton').hover(
		function () {
			$(this).stop().animate({ 'background-color': '#d4d4d4' }, 300);
		},
		function () {
			$(this).stop().animate({ 'background-color': '#ffffff' }, 300);
		}
	);

	/* Выравнивание формы */

	if ($(window).width() < 1000) { $('.b-contentItemRight').css({ 'left': '500px' }); }
	if ($(window).width() > 1000) { $('.b-contentItemRight').css({ 'left': '50%' }); }

	$(window).resize(function () {
		if ($(window).width() < 1000) { $('.b-contentItemRight').css({ 'left': '500px' }); }
		if ($(window).width() > 1000) { $('.b-contentItemRight').css({ 'left': '50%' }); }
	});

	/* Фиксы для IE8 */

	if ($.browser.msie) {
		if ($.browser.version >= 8.0 && $.browser.version < 9.0) {
			$('.b-contentHolder').css({ 'background': '#fff' });
			$('.b-contentItem:nth-child(1) .b-contentTitle, .b-contentItem:nth-child(1) .b-contentTopText, .b-contentItem:nth-child(1) .b-contentText').css({ 'color': '#000' });

			$('.b-content').css({'height':'576px'});
			$('.b-top .b-topMenuItem:nth-child(1)').css({
				'background': '#003366',
				'width': '130px',
				'padding': '10px 0 0 36px',
				'height': '44px',
				'background': '#fff url(../img/topArrow.png) no-repeat 6px 8px',
				'border': '1px solid #a5a5a5',
				'color': '#7b7979'
			});
			$('.b-top .b-topMenuItem:nth-child(2)').css({ 'background': '#003366' });
			$('.b-top .b-topMenuItem:nth-child(3)').css({ 'background': '#009e49' });
			$('.b-top .b-topMenuItem:nth-child(4)').css({ 'background': '#009cff' });
			$('.b-top .b-topMenuItem:nth-child(5)').css({ 'background': '#68217a' });
			$('.b-top .b-topMenuItem:nth-child(6)').css({ 'background': '#b4009e' });

			$('.b-bottom .b-topMenuItem:nth-child(1)').css({ 'display': 'none' });
			$('.b-bottom .b-topMenuItem:nth-child(2)').css({ 'background': '#003366' });
			$('.b-bottom .b-topMenuItem:nth-child(2)').hover(
				function () {
					$(this).css({ 'background': '#003366' });
				},
				function () {
					$(this).css({ 'background': '#003366' });
				}
			);

			$('.b-bottom .b-topMenuItem:nth-child(3)').css({ 'background': '#009e49' });
			$('.b-bottom .b-topMenuItem:nth-child(3)').hover(
				function () {
					$(this).css({ 'background': '#009e49' });
				},
				function () {
					$(this).css({ 'background': '#009e49' });
				}
			);

			$('.b-bottom .b-topMenuItem:nth-child(4)').css({ 'background': '#009cff' });
			$('.b-bottom .b-topMenuItem:nth-child(4)').hover(
				function () {
					$(this).css({ 'background': '#009cff' });
				},
				function () {
					$(this).css({ 'background': '#009cff' });
				}
			);

			$('.b-bottom .b-topMenuItem:nth-child(5)').css({ 'background': '#68217a' });
			$('.b-bottom .b-topMenuItem:nth-child(5)').hover(
				function () {
					$(this).css({ 'background': '#68217a' });
				},
				function () {
					$(this).css({ 'background': '#68217a' });
				}
			);

			$('.b-bottom .b-topMenuItem:nth-child(6)').css({ 'background': '#b4009e' });
			$('.b-bottom .b-topMenuItem:nth-child(6)').hover(
				function () {
					$(this).css({ 'background': '#b4009e' });
				},
				function () {
					$(this).css({ 'background': '#b4009e' });
				}
			);


			$('.b-checkLabel .pseudo-check').css({'background': 'url(img/checkbox.png) -20px top no-repeat'})

			if ($('.b-checkbox1 .b-checkInput1').is(':checked'))
			{
				
				$('.b-checkLabel .pseudo-check').css({'background': 'url(img/checkbox.png) -20px top no-repeat'})
			}
			else {
				$('.b-checkLabel .pseudo-check').css({'background': 'url(img/checkbox.png) left top no-repeat'})
			}

			$('.b-checkLabel .pseudo-check').click(function(){
				if ($('.b-checkbox1 .b-checkInput1').is(':checked'))
				{
					$('.b-checkLabel .pseudo-check').css({'background': 'url(img/checkbox.png) left top no-repeat'})
					
				}
				else {
					$('.b-checkLabel .pseudo-check').css({'background': 'url(img/checkbox.png) -20px top no-repeat'})
				}
			});

			


			$('.pseudo-radio').css({'background': 'url(img/checkbox.png) left top no-repeat'});

			if ($('.b-radioInput').is(':checked'))
			{
				
				$(this).parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'});
			}
			else {
				$('.pseudo-radio').css({'background': 'url(img/checkbox.png) left top no-repeat'});
			}

			$('.pseudo-radio').click(function(){
				if ($('.b-radioInput').is(':checked'))
				{	
					$('.pseudo-radio').css({'background': 'url(img/checkbox.png) left top no-repeat'});
					$(this).parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) -20px top no-repeat'})
					
				}
				else {
					
					$('.b-radioInput').parent().find('.pseudo-radio').css({'background': 'url(img/checkbox.png) left top no-repeat'});
				}
			});

			


			
			


		}
	}


});