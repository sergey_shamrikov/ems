﻿(function ($) {
	$.bi = {
		dataConsumers: [],
		dataRetrievers: [],
		dataManipulators: [],

		isInitialized: function () {
			return this._isInitialized;
		},

		loadEnabled: function () {
			if (arguments.length >= 1 && typeof arguments[0] == 'boolean')
				this._loadEnabled = arguments[0];
			return this._loadEnabled;
		},

		ignoreAttr: function (attr) {
			if (typeof attr == 'string' && $.trim(attr) != '') {
				if ($.inArray(attr, this._ignoreAttr) == -1)
					this._ignoreAttr.push(attr);
			} else if (typeof attr == 'object') {
				for (var i = 0; i < attr.length; i++) {
					attr[i] = $.trim(attr[i]);
					if ($.inArray(attr[i], this._ignoreAttr) == -1)
						this._ignoreAttr.push(attr[i]);
				}
			}
		},

		ignoreTextClass: function (cssClasses) {
			if (typeof cssClasses == 'string' && $.trim(cssClasses) != '') {
				var classes = cssClasses.split(/\s+/);
				for (var i = 0; i < classes.length; i++) {
					if ($.inArray(classes[i], this._ignoreTextClass) == -1)
						this._ignoreTextClass.push(classes[i]);
				}
			}
		},

		baseData: function () {
			if (arguments.length >= 1 && typeof arguments[0] == 'object')
				$.extend(this._baseData, this._objectNametoLower(arguments[0]));
			return this._baseData;
		},

		baseLoadData: function () {
			if (arguments.length >= 1 && typeof arguments[0] == 'object')
				$.extend(this._baseLoadData, this._objectNametoLower(arguments[0]));
			return this._baseLoadData;
		},

		baseLinkData: function () {
			if (arguments.length >= 1 && typeof arguments[0] == 'object')
				$.extend(this._baseLinkData, this._objectNametoLower(arguments[0]));
			return this._baseLinkData;
		},

		_objectNametoLower: function (obj) {
			// Convert to lower case the top level object names item so it will prevent errors caused by miscapitalized letters
			var newObj = {};
			for (var i in obj) {
				newObj[i.toLowerCase()] = obj[i];
			}
			return newObj;
		},

		// Core -----------------------------------------------------------------------
		init: function (settings) {
			///	<summary>
			///		Initializes BI for the page.
			///	</summary>
			///	<param name="mapping" type="BiMapping">
			///		Optional parameter that takes in JSON object to map the providers to their bi parameters/values.
			///	</param>

			// Prevent from being reinitialized


			if (this.isInitialized()) return;
			this._isInitialized = true;

			this._ns = this.getNamespacePrefix('urn:schemas-microsoft-com:mscom:bi');

			this.ignoreAttr(this._ns + ':track');

			var eventName = (window.attachEvent ? 'click' : 'mousedown'); // IE: click, Other Browsers: mousedown
			$('a, area').live(eventName, $.proxy(function (event) {
				var biEnabled = true; // Tracking enabled by default
				var $el = $(event.currentTarget);

				// Find closeset bi:track attribute
				var currentAttr = $el.attr($.bi._ns + ':track');
				currentAttr = (currentAttr) ? currentAttr.toLowerCase() : currentAttr;
				if (currentAttr && (currentAttr === 'true' || currentAttr === 'false')) {
					// Check element with attribute
					biEnabled = (currentAttr == 'true');
				} else {
					var parents = $el.parents(':not(table)').filter(function () {
						// Get all ancestors with attribute
						var currentAttr = $(this).attr($.bi._ns + ':track');
						currentAttr = (currentAttr) ? currentAttr.toLowerCase() : currentAttr;
						return (currentAttr && (currentAttr === 'true' || currentAttr === 'false')) ? true : false;
					});
					if (parents.length) {
						// Get closest ancestors with attribute
						biEnabled = ($(parents[0]).attr($.bi._ns + ':track').toLowerCase() == 'true');
					}
				}
				if (biEnabled) {
					var target = event.target;
					if (target.tagName.toLowerCase() == 'area') {
						// Set target to the corresponding img
						target = $("img[usemap='#" + $(target).parent('map').attr('name') + "']").get(0);
					}
					this.linkClick(event.currentTarget, target);
				}
			}, this));

			// Call init() for Data Retrievers
			for (var n in this.dataRetrievers) {
				try {
					this.dataRetrievers[n].init();
				} catch (e) { }
			}

			// Call init() for Data Consumers
			for (var n in this.dataConsumers) {
				try {
					this.dataConsumers[n].init(settings[n]);
				} catch (e) { }
			}

			if (this.loadEnabled())
				$($.proxy(this.recordLoad, this));
		},

		record: function (data) {
			///	<summary>
			///		Sends a beacon containing the parameters that are mapped to each provider .
			///	</summary>
			///	<param name="parameters" type="JSON">
			///		Key-value pair JSON object of parameter names/values .
			///	</param>

			data = $.extend({}, this._baseData, data);

			// Call record fn for Data Manipulator
			for (var n in this.dataManipulators) {
				try {
					this.dataManipulators[n].preRecord(data);
				} catch (e) { }
			}

			// Call record fn for Data Consumers
			for (var n in this.dataConsumers) {
				try {
					this.dataConsumers[n].record(data);
				} catch (e) { }
			}
		},

		recordLoad: function (data) {
			///	<summary>
			///		Sends a beacon for page load containing parameters that are mapped to each provider.
			///	</summary>

			if (data == $)
				data = {};

			data = $.extend({}, this._baseData, this._baseLoadData, data);

			for (var n in this.dataRetrievers) {
				try {
					$.extend(data, this.dataRetrievers[n].getLoadData());
				} catch (e) { }
			}

			// Call record fn for Data Manipulator
			for (var n in this.dataManipulators) {
				try {
					this.dataManipulators[n].preRecordLoad(data);
				} catch (e) { }
			}

			// Call record fn for Data Consumers
			for (var n in this.dataConsumers) {
				try {
					this.dataConsumers[n].recordLoad(data);
				} catch (e) { }
			}
		},

		linkClick: function (element, target) {
			///	<summary>
			///		1: $(element) - Sends beacon with bi information for an element.
			///		2: $(element, target) - Sends beacon with bi information for an element and its target (for use with images and map areas).
			///	</summary>
			///	<param name="element" type="Element">
			///		1: element - A DOM Element.
			///		2: element - A DOM Element.
			///	</param>
			///	<param name="target" type="Element">
			///		2: target - A DOM Element.
			///	</param>


			this.record(this.getLinkData(element, target));
		},

		getLinkData: function (element, target) {
			///	<summary>
			///		1: $(element) - gets bi information for an element. Retrieves all the link data from the Retriever plugins
			///		2: $(element, target) - get bi information for an element and its target (for use with images and map areas). 
			///	</summary>
			///	<param name="element" type="Element">
			///		1: element - A DOM Element.
			///		2: element - A DOM Element.
			///	</param>
			///	<param name="target" type="Element">
			///		2: target - A DOM Element.
			///	</param>
			var data = $.extend({}, this._baseData, this._baseLinkData);

			for (var n in this.dataRetrievers) {
				try {
					$.extend(data, this.dataRetrievers[n].getLinkData(element, target));
				} catch (e) { }
			}
			return data;
		},
		// Helpers --------------------------------------------------------------------
		getAttrData: function (element, includeIgnoredAttr) {
			///	<summary>
			///		1: $(element) - Gets the bi data for an element.
			///		2: $($elements) - Gets the bi data for a set of jquery elements and merges them.
			///	</summary>
			///	<param name="element" type="Element/Jquery">
			///		1: element - A DOM Element.
			///		2: $elements - Jquery Element.
			///	</param>
			///	<returns type="JSON: name-value[] pair" />

			//TODO: Allow data attributes to be placed in other namespaces. Ones in the HTML namepace would need to be registered.

			var data = {};
			var attr, attrName;
			if (jQuery && element instanceof jQuery) {
				// Get data for a set of jquery selectors
				var $elements = element;
				for (var h = 0, len1 = $elements.length; h < len1; h++) {
					var attributes = $elements.get(h).attributes;
					for (var i = 0, len2 = attributes.length; i < len2; i++) {
						attr = attributes.item(i);
						attrName = attr.name.toLowerCase();
						// Only get attributes in the urn:schemas-microsoft-com:mscom:bi namespace and ignore ones in this._ignoreAttr array
						if ((attrName.indexOf($.bi._ns + ':') === 0) && (includeIgnoredAttr || $.inArray(attrName, this._ignoreAttr) < 0)) {
							index = attrName.substring(3, attr.name.length);
							if (data[index] == undefined) {
								data[index] = [];
							}
							if ($.inArray(attr.value, data[index]) < 0) {
								data[index].push(attr.value);
							}
						}
					}
				}
				for (var i in data) {
					data[i] = data[i].join(';');
				}

			} else {
				// Get data for a single dom element
				var attributes = element.attributes;

				for (var i = 0, len = attributes.length; i < len; i++) {
					attr = attributes.item(i);
					// Only get attributes in the urn:schemas-microsoft-com:mscom:bi namespace and ignore ones in this._ignoreAttr array
					if ((attr.name.indexOf($.bi._ns + ':') === 0) && (includeIgnoredAttr || $.inArray(attr.name, this._ignoreAttr) < 0)) {
						data[attr.name.toLowerCase().substring(3, attr.name.length)] = attr.value;
					}
				}
			}
			return data;
		},

		getText: function (element) {
			///	<summary>
			///		Returns the text of an element while ignoring text in certian elements
			///	</summary>
			///	<param name="element" type="Element">
			///		1. Element
			///	</param>
			///	<returns type="Text: string text" />

			// Use cloning hack due to bug in jQuery clone() that causes IE6/7 to crash when calling more than once
			var innerHtml = $(element).html();
			var $clone = $('<div>' + innerHtml + '</div>');
			if (this._ignoreTextClass.length > 0) {
				var classSelector = '.' + this._ignoreTextClass.join(',.');
				$clone.find(classSelector).remove();
			}
			return $.trim($clone.text());
		},

		mapData: function (data, map) {
			if (!map) return false;

			var result = {};

			for (var i in map) {
				result[i] = '';
				for (var j = 0, l = map[i].length; j < l; j++) {
					if (map[i][j].str !== undefined) {
						result[i] += map[i][j].str;
					} else if (map[i][j].bi && data && data[map[i][j].bi] !== undefined) {
						// Reset the domain, path, and query
						switch (map[i][j].bi) {
							case 'uripath':
								if (data[map[i][j].bi] == '' || data[map[i][j].bi] == null || data[map[i][j].bi] == undefined) {
									data[map[i][j].bi] = '/';
								} else if (data[map[i][j].bi].substring(0, 1) != '/') {
									data[map[i][j].bi] = '/' + data[map[i][j].bi];
								}
								break;
							case 'uriquery':
								if (data[map[i][j].bi] == '' || data[map[i][j].bi] == null || data[map[i][j].bi] == undefined) {
									data[map[i][j].bi] = '?';
								}
								break;
						}
						// Todo Handle the data = null/data == undefined here
						result[i] += data[map[i][j].bi];
					}
				}

				// Remove empty data
				if (result[i] == '')
					delete result[i];
			}
			return result;
		},

		isInteractionTypeValid: function (data, mapping) {
			///	<summary>
			///		Checks if parameter is valid based on the provider settings (ie InteractionType whitelist)
			///		Checks also if provider itself is enabled(ie InteractionType whitelist)
			///	</summary>
			///	<param name="params" type="JSON: name-value pair">
			///		Message.
			///	</param>        
			///	<param name="mapping" type="Element">
			///		the mapping object containing the interactiontype white list
			///	</param>
			///	<returns type="Boolean:true if valid" />

			if (!data || !mapping) {
				return false;
				/*} else if (mapping.settings && mapping.settings.interactiontype) { // backwards compatibility
				return ((!data.interactiontype) || (mapping.settings && mapping.settings.interactiontype && mapping.settings.interactiontype[data.interactiontype] != undefined));
				*/
			} else if (mapping.interactionTypeMap) { // mapping.interactionTypeMap
				return ((!data.interactiontype) || (mapping.interactionTypeMap[data.interactiontype] != undefined));
			} else {
				return false;
			}

		},

		getNamespacePrefix: function (namespace) {
			// Capture the HTML namespace prefix for a specific namespace
			if (document.namespaces) {
				var namespaces = document.namespaces;
				for (var i = 0; i < namespaces.length; i++) {
					if (namespaces[i].urn == namespace)
						return namespaces[i].name;
				}
			}
			var attr = $('html').get(0).attributes;
			for (var i = 0; i < attr.length; i++) {
				if (attr[i].value == namespace) {
					if (attr[i].name.indexOf('xmlns:') == 0) {
						return attr[i].name.substring(attr[i].name.indexOf(':') + 1);
					}
				}
			}
		},

		// Internals ------------------------------------------------------------------
		_isInitialized: false,
		_ns: 'bi', // HTML namespace prefix
		_loadEnabled: true,

		_ignoreAttr: [],
		_ignoreTextClass: [],

		_baseData: {}, // Base data for beacons
		_baseLoadData: { interactiontype: 0 }, // Base data for load beacon
		_baseLinkData: { interactiontype: 2 } // Base data for automatic link click beacons         
	};
})(jQuery);

(function ($) {
	$.bi.dataConsumers.webtrends = {
		_settings: null,

		init: function (settings) {
			this._settings = settings;
			// Create WebTrends object
			if (typeof window.WebTrends != 'undefined') {
				this.WebTrends = new WebTrends();
			}
		},

		record: function (data) {
			if (this.WebTrends && $.bi.isInteractionTypeValid(data, this._settings)) {
				data = this._mapInteractionType(data);
				this.WebTrends.dcsJSONTrack({}, this._map(data));
			}
		},

		recordLoad: function (data) {
			if (this.WebTrends && $.bi.isInteractionTypeValid(data, this._settings)) {
				try { this.WebTrends.dcsCollect(); } catch (e) { }
				data = this._mapInteractionType(data);

				this.WebTrends.dcsJSONTrack(
				{
					'DCS.dcssip': window.location.hostname,
					'DCS.dcsuri': window.location.pathname,
					'DCS.dcsqry': window.location.search
				}, this._map(data));
			}
		},

		_mapInteractionType: function (dataByRef) {
			var data = $.extend({}, dataByRef); // Break the object reference
			// Map the interaction type
			if (data['interactiontype'] != undefined && this._settings.interactionTypeMap[data['interactiontype']] != undefined) {
				data['interactiontype'] = this._settings.interactionTypeMap[data['interactiontype']];
			}
			// Map the trigger interaction type
			if (data['triggertype'] != undefined && this._settings.interactionTypeMap[data['triggertype']] != undefined) {
				data['triggertype'] = this._settings.interactionTypeMap[data['triggertype']];
			}
			return data;
		},

		_map: function (data) {
			return $.bi.mapData(data, this._settings.parameterMap);
		}
	};
})(jQuery);

(function ($) {
	$.bi.dataConsumers.wedcs = {
		_settings: null,

		init: function (settings) {
			this._settings = settings;
			$.bi.baseLinkData({ cot: 1 }); // TODO: we can make dataconsumer specific
		},

		record: function (data) {
			if (window.MscomCustomEvent && $.bi.isInteractionTypeValid(data, this._settings)) {
				data = this._mapInteractionType(data);
				MscomCustomEvent.apply(this, this._map(data));
			}
		},

		recordLoad: function (data) {
			if (window.MscomCustomEvent && $.bi.isInteractionTypeValid(data, this._settings)) {
				data = $.extend(data, { 'cot': 0 });
				data = this._mapInteractionType(data);
				MscomCustomEvent.apply(this, this._map(data));
			}
		},

		_mapInteractionType: function (dataByRef) {
			var data = $.extend({}, dataByRef); // Break the object reference
			//TODO: mapping.settings.interactiontype was changed to mapping.interactionTypeMap { 1:3} in this version. BiHandler needs to be Updated to support this we need to make this backwards compatible

			/*// Interaction type mapping to make it Backwards compatible
			var it = { 0: 0, 1: 4, 2: 1, 3: 2, 4: 9, 5: 10, 6: 11, 7: 12, 8: 13, 9: 14, 10: 15, 11: 16, 12: 17, 13: 18, 14: 19, 15: 20, 16: 21, 17: 22, 18: 23, 19: 24, 20: 25 };
			*/

			// Map the interaction type
			if (this._settings.interactionTypeMap && data['interactiontype'] && this._settings.interactionTypeMap[data['interactiontype']] != undefined) {
				data['interactiontype'] = this._settings.interactionTypeMap[data['interactiontype']];
			} /* else if (data['interactiontype'] && it[data['interactiontype']] != undefined) { // Backwards compatible
								data['triggertypeinteractiontype'] = it[data['interactiontype']];
						}*/

			// Map the trigger interaction type
			if (this._settings.interactionTypeMap && data['triggertype'] && this._settings.interactionTypeMap[data['triggertype']] != undefined) {
				data['triggertype'] = this._settings.interactionTypeMap[data['triggertype']];
			} /* else if (data['triggertype'] && it[data['triggertype']] != undefined) { // Backwards compatible
								data['triggertype'] = it[data['triggertype']];
						}*/

			return data;
		},

		_map: function (dataByRef) {
			var data = $.extend({}, dataByRef); // Clones the object and prevents pass by ref
			if (data['uridomain'] == undefined) {
				data['uridomain'] = window.location.hostname;
				data['uripath'] = window.location.pathname;
				data['uriquery'] = window.location.search;
			}
			return this._toArray($.bi.mapData(data, this._settings.parameterMap));
		},

		_toArray: function (json) {
			// Converts a JSON object into an array of key-value pairs
			json = (!json || typeof json != 'object' || json.sort) ? {} : json;
			var array = [];
			for (var key in json) {
				if (json.hasOwnProperty(key)) {
					array.push(key);
					array.push(json[key]);
				}
			}
			return array;
		}
	};
})(jQuery);

(function ($) {
	$.bi.dataRetrievers.attr = {
		getLoadData: function () {
			var $items;
			// some bugs in firefox if $('area:visible') is used and it doesnt work completely in ie
			$items = $('area').filter(function () {
				//TODO: check for an image with usemap attribute that matches the map name
				return $(this).parent('map').siblings('img').is(':visible');
			});
			// Merge visible area elements with visible anchor elements
			$items = $items.add($('a:visible'));

			return $.bi.getAttrData($items);
		},

		getLinkData: function (element, target) {
			return $.bi.getAttrData(element);
		}
	};
})(jQuery);

//TODO: support multiple experiments
(function ($) {
	if ($.bi !== undefined) {
		$.bi.dataRetrievers.exp = {
			globalVarName: 'csp_expData',
			init: function () {
				if (window[this.globalVarName] != undefined && window[this.globalVarName][0] != undefined && typeof window[this.globalVarName] == 'object') {
					var baseData = {};

					var processItem = function (name, value) {
						if (typeof value == 'string' || typeof value == 'number' || typeof value == 'boolean') {
							baseData[name] = value;
						} else if (typeof value == 'object') {
							// Process sub objects
							processObject(name, value);
						}
					};

					var processObject = function (prefix, data) {
						for (var i in data) {
							name = prefix + '_' + i;
							processItem(name, data[i]);
						}
					};

					processObject('exp', window[this.globalVarName][0]);

					// Read uid value from MS_WT cookie
					var cookies = document.cookie.split(';');
					var j1, n1;
					for (var i1 in cookies) {
						// Get the index of the '='
						j1 = cookies[i1].indexOf('=');

						// Get the name of the cookie
						n1 = cookies[i1].substr(0, j1);

						if ($.trim(n1) == 'MS_WT') {
							var cookie = cookies[i1].substr(j1 + 1);

							// Split the cookie at the '&' character
							var experiments = cookie.split('&');
							var j2, n2;
							for (var i2 in experiments) {
								// Get the index of the '='
								j2 = experiments[i2].indexOf('=');

								// Get the name of the experiment
								n2 = experiments[i2].substr(0, j2);

								if (baseData['exp_experimentID'] != undefined && $.trim(n2) == baseData['exp_experimentID']) {
									var value = experiments[i2].substr(j2 + 1);

									var uid = this._getCookieValue('uid', value);
									if (uid) {
										baseData['exp_uid'] = uid;
									}

									var typeid = this._getCookieValue('typeid', value);
									var testid = this._getCookieValue('testid', value);
									var runid = this._getCookieValue('runid', value);
									var trackid = this._getCookieValue('trackid', value);

									var currentPath = this._getCookieValue('currentPath', value);
									currentPath = currentPath ? (currentPath.split('-')[0]) : currentPath; // get string before '-' character

									if (typeid && currentPath && testid && runid && trackid) {
										baseData['exp_trackingguid'] = [typeid, currentPath, testid, runid, trackid].join('-');
									}
									break;
								}
							}
							break;
						}
					}

					$.bi.baseData(baseData);
				}
			},
			_getCookieValue: function (name, searchString) {

				// For use with strings folloing the format: \\\"name\\\":\\\"value\\\"

				var result = new RegExp('(\\\\\\\\\\\\"' + name + '\\\\\\\\\\\\":\\\\\\\\\\\\")([^"\\\\]*)(\\\\\\\\\\\\")', "i").exec(searchString);
				return result ? result[2] : false;
			}
		};
		$.bi.dataManipulators.exp = {
			conversionPointName: 'cpid',
			preRecord: function (data) {
				//Remove the conversion point for non click events

				if (data && (data['interactiontype'] == 0 || data['interactiontype'] == 1)) {
					this._clearConversionPoint(data);
				}
			},
			preRecordLoad: function (data) {
				this._clearConversionPoint(data);
			},
			_clearConversionPoint: function (data) {
				if (data && data[this.conversionPointName] != undefined) {
					delete data[this.conversionPointName];
				}
			}
		};
	}
})(jQuery);

(function ($) {
	$.bi.dataRetrievers.structure = {
		init: function () {
			$.bi.ignoreAttr($.bi._ns + ':titleflag');
			$.bi.ignoreAttr($.bi._ns + ':parenttitle');
			$.bi.ignoreAttr($.bi._ns + ':title');
			$.bi.ignoreAttr($.bi._ns + ':index');
			$.bi.ignoreAttr($.bi._ns + ':gridindex');
			$.bi.ignoreAttr($.bi._ns + ':gridtype');
			$.bi.ignoreAttr($.bi._ns + ':type');

			$.bi.baseLoadData({ title: document.title, initial: 0 });
		},

		getLinkData: function (element, target) {
			///	<summary>
			///		1: $(element) - Gets a link's bi information.
			///		2: $(element, target) - Gets a link's bi information (for use with images and map areas).
			///	</summary>
			///	<param name="element" type="Element">
			///		1: element - A DOM Element.
			///		2: element - A DOM Element.
			///	</param>
			///	<param name="target" type="Element">
			///		2: target - A DOM Element.
			///	</param>

			var data = {};
			var hasTagName = (element && $(element).length && $(element).get(0).tagName);
			var hrefStr = (hasTagName) ? $(element).attr('href') : '';
			// Add standard link params like title hrefW
			data['urifull'] = hrefStr;
			data['uridomain'] = (hasTagName && $(element)[0].hostname) ? $(element)[0].hostname : '';
			data['uripath'] = (hasTagName && $(element)[0].pathname) ? $(element)[0].pathname : '';
			data['uripath'] = (data['uripath'].length && data['uripath'].indexOf('/') != 0) ? ('/' + data['uripath']) : data['uripath']; // make it consistent 
			data['urihash'] = (hrefStr && hrefStr.indexOf('#') >= 0) ? (hrefStr.substring(hrefStr.indexOf('#'))) : '';

			// handle the links within a page "#foo"
			if (hrefStr.indexOf('#') == 0) {
				data['uriquery'] = window.location.search;
			} else {
				hrefStr = (data['urihash']) ? (hrefStr.substring(0, hrefStr.indexOf('#'))) : hrefStr;
				data['uriquery'] = (hrefStr && hrefStr.indexOf('?') >= 0) ? (hrefStr.substring(hrefStr.indexOf('?'))) : "";
			}

			// Current Type if available
			data['type'] = (hasTagName && $(element).attr($.bi._ns + ':type')) ? $.trim($(element).attr($.bi._ns + ':type')) : '';
			// Current Index if available bi:index on the A
			data['index'] = (hasTagName && $(element).attr($.bi._ns + ':index')) ? $.trim($(element).attr($.bi._ns + ':index')) : '';

			data['linktitle'] = $.bi.getText(element);
			data['title'] = data['linktitle'];

			var nodeName = (element && $(element).length && $(element)[0].nodeName) ? $(element).get(0).nodeName.toLowerCase() : '';
			if (nodeName == 'area') {
				data['linktitle'] = data['title'] = $(element).attr('alt');
			} else if (data['title'] == '' && target && $(target).get(0).nodeName == 'IMG') {
				data['title'] = $(target).attr('alt');
			}

			$.extend(data, this.getData(element));
			return data;
		},

		getData: function (element) {
			var params = {};
			params['parenttitlestructure'] = this.getTitleStructure(element, null, true);
			$.extend(params, this.getIndexGridTypeStructure(element));
			return params;
		},

		getIndexGridTypeStructure: function (element) {
			var combined = {},
			grids = [],
			items = [],
			types = [];
			$(element).parents(':not(table)').each(function () {
				// Grid Logic
				if ($(this).attr($.bi._ns + ':gridindex')) {
					var prefix = ($(this).attr($.bi._ns + ':gridtype')) ? $(this).attr($.bi._ns + ':gridtype') : '';
					grids.push(prefix + ' ' + $(this).attr($.bi._ns + ':gridindex'));
				}
				// Item 
				if ($(this).attr($.bi._ns + ':index')) {
					items.push($(this).attr($.bi._ns + ':index'));
				}
				// Type
				if ($(this).attr($.bi._ns + ':type')) {
					types.push($.trim($(this).attr($.bi._ns + ':type')));
				}
			});

			combined['parenttypestructure'] = types.reverse().join(';');
			combined['gridstructure'] = grids.reverse().join(';');
			combined['parentindexstructure'] = items.reverse().join(';');

			return combined;
		},

		getIndexStructure: function (element) {
			// Traverse the DOM upwards and return a semicolon delimited string of bi:index elements
			var types = [];
			var cspname = false;
			$(element).parents(':not(table)').filter(function () {
				if ($(this).attr($.bi._ns + ':index')) {
					types.push($(this).attr($.bi._ns + ':index'));
					return true;
				}
				return false;
			});

			return types.reverse().join(';');
		},

		getGridStructure: function (element) {
			// Traverse the DOM upwards and return a semicolon delimited string of bi:griditem elements the prefix will be determined by the class
			var types = [];
			$(element).parents(':not(table)').filter(function () {
				if ($(this).attr($.bi._ns + ':gridindex')) {
					var prefix = ($(this).attr($.bi._ns + ':gridtype')) ? $(this).attr($.bi._ns + ':gridtype') : '';
					types.push(prefix + ' ' + $(this).attr($.bi._ns + ':gridindex'));
					return true;
				}
				return false;
			});
			// Reverse
			return types.reverse().join(';');
		},

		getTypeStructure: function (element) {
			// Traverse the DOM upwards and return a semicolon delimited string of CSP types (can be master, page, and/or component).
			var types = [];
			$(element).parents(':not(table)').filter(function () {
				if ($(this).attr($.bi._ns + ':type')) {
					types.push($.trim($(this).attr($.bi._ns + ':type')));
					return true;
				}
				return false;
			});

			return types.reverse().join(';');
		},

		getTitleStructure: function (element, recurse, parentOnly) {
			var names = [],
			biParent = [],
			$biFlag,
			$biItem,
			biName = '';


			if (!element || !$(element).length || !$(element).get(0).tagName) {
				return "";
			}
			//1. Get the text on the link add to the array
			if (!parentOnly) { // True if you just want the parent
				names.push(($.trim($(element).text()) || $.trim($(element).attr('alt'))));
			}
			/*2. Search for the first bi:parenttitles="name1" upon finding one (stop the current loop). search for the sibling/parent for bi:titleflag="name1"*/
			// Add first Item if the element exists
			if ($(element).attr($.bi._ns + ':parenttitle')) {
				biParent.push($(element));
			}
			$(element).parents(':not(table)').each(function () {
				if ($(this).attr($.bi._ns + ':parenttitle')) {
					biParent.push($(this));
				}
			});

			/* Update: made this more robust. Checking bi:parenttitle on the other parernttitle until it finds one that works*/
			for (var cnt = 0; cnt < biParent.length; cnt++) {
				var $thisParent = biParent[cnt];
				biName = $thisParent.attr($.bi._ns + ':parenttitle');

				// Search Sibling
				$biFlag = $thisParent.siblings(':not(table)').filter(function () {
					return ($(this).attr($.bi._ns + ':titleflag') == biName);
				}).first();
				if ($biFlag.length == 0) {
					$biFlag = $thisParent.parents(':not(table)').filter(function () {
						return ($(this).attr($.bi._ns + ':titleflag') == biName);
					}).first();
				}
				if ($biFlag.length != 0) {
					/* 3. Search for the bi:title="name1" on the current item or children
					upon finding it, get the text add to the array
					- continue to 1, look for bi:parenttitle
					*/
					if ($biFlag.attr($.bi._ns + ':title') == biName) { // current element
						$biItem = $biFlag;
					} else { // search children
						$biItem = $biFlag.find('[' + $.bi._ns + '\\:title="' + biName + '"]').first();
						// Bug in jquery 1.4.4 for find in IE7 using this as a fallback
						if ($biItem.length == 0) {
							$biItem = $biFlag.find('*').filter(function () {
								return ($(this).attr($.bi._ns + ':title') == biName);
							}).first();
						}
					}

					if ($biItem) {
						// Recurse function if parent exists
						var results = this.getTitleStructure($biItem, true);
						names = names.concat(results);
						break; // stop the current loop if the title is already found
					}
				}
			}

			return (recurse) ? names : names.reverse().join(';');
		}
	};
})(jQuery);

(function ($) {
	$.bi.queue = {
		_delay: 350,
		_timerId: null,
		_queue: [],
		push: function (params) {
			this._queue.push(params);
			if (this._timerId == null)
				this._startTimer();
		},
		_tryProcess: function () {
			this._clearTimer();

			// Check if anything is animating
			if ($(':animated').length == 0 && this._queue.length > 0)
				$.bi.record(this._queue.shift());

			if (this._queue.length > 0)
				this._startTimer();
		},
		_startTimer: function () {
			this._clearTimer();
			this._timerId = window.setTimeout($.proxy(this._tryProcess, this), this._delay);
		},
		_clearTimer: function () {
			if (this._timerId != null) {
				window.clearTimeout(this._timerId);
				this._timerId = null;
			}
		}
	};
})(jQuery);
