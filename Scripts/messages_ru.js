/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: RU (Russian; русский язык)
 */
(function ($) {
	$.extend($.validator.messages, {
		required: "Нужно выбрать один из четырех пакетов",
		name: "Введите ваше Ф.И.О.",
		phone: "Введите ваш телефон",
		radio: "Нужно выбрать один из четырех пакетов",
		check: "Необходимо ваше согласие на обработку данных"
	});
}(jQuery));